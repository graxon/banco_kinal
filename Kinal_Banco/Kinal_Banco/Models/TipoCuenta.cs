﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Kinal_Banco.Models
{
    public class TipoCuenta
    {
        public int id { get; set; }
        public string cuentaTipo { get; set; }

        public int Cuentaid { get; set; }
        public Cuenta Cuenta { get; set; }
    }
}