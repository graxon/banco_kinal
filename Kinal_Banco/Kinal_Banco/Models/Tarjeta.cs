﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kinal_Banco.Models
{
    public class Tarjeta
    {
        public int id { get; set; }
        public int idTipoTarjeta { get; set; }
        public int numeroTarjeta { get; set; }
        public int PinAcceso { get; set; }
        public string FechaCreacion { get; set; }
        public string FechaValidez { get; set; }
        public decimal Cantidad { get; set; }
        public int numeroSecreto { get; set; }
        public string FechaPago { get; set; }

        public int Cuentaid { get; set; }
        public Cuenta Cuenta { get; set; }
    }
}