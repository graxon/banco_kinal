﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Kinal_Banco.Models
{
    public class TipoMovimiento
    {
        public int id { get; set; }
        public string cheque { get; set; }
        public string gastos { get; set; }
        public decimal transferencia { get; set; }


        public int Movimientoid { get; set; }
        public Movimiento Movimiento { get; set; }
    }
}