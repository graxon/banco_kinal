﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Kinal_Banco.Models
{
    public class Cuenta
    {
        public int id { get; set; }
        public int idTipoCuenta { get; set; }
        public decimal Saldo { get; set; }
        public int numeroCuenta { get; set; }


        public int Personaid { get; set; }
        public Persona Persona { get; set; }
    }
}
