﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Kinal_Banco.Models
{
    public class Rol
    {
        public int id { get; set; }
        public string nombre { get; set; }

        public int Usuarioid { get; set; }
        public Usuario Usuario { get; set; }
    }
}