﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Kinal_Banco.Models
{
    public class Persona
    {

        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public int dpi { get; set; }
        public int telefono { get; set; }
        public string correo { get; set; }
        public string nombreReferencia { get; set; }
        public int telefonoReferencia { get; set; }

        public string nombreReferencia2 { get; set; }
        public int telefonoReferencia2 { get; set; }


    }
}