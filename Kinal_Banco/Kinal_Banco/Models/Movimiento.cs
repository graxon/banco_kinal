﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Kinal_Banco.Models
{
    public class Movimiento
    {
        public int id { get; set; }
        public int idCuenta { get; set; }
        public string Lugar { get; set; }
        public decimal cantidadConsumida { get; set; }
        public decimal cantidadRetirada { get; set; }
        public DateTime fechaReterido { get; set; }
        public DateTime fechaRealizado { get; set; }
        public string hora { get; set; }


        public int Transaccionid { get; set; }
        public Transaccion Transaccion { get; set; }
    }
}