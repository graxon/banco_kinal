﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Kinal_Banco.Models
{
    public class Usuario
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public DateTime fechaCreacion { get; set; }
        public string estado { get; set; }

        public int Personaid { get; set; }
        public Persona Persona { get; set; }

    }
}