﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Kinal_Banco.Models
{
    public class Transaccion
    {
        public int id { get; set; }
        public int idCuenta { get; set; }
        public string Lugar { get; set; }
        public string hora { get; set; }
        public string fechaRealizado { get; set; }
        public decimal SaldoAbonado { get; set; }
        public decimal SaldoRetirado { get; set; }

        public int Cuentaid { get; set; }
        public Cuenta Cuenta { get; set; }
    }
}