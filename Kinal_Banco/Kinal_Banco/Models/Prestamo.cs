﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Kinal_Banco.Models
{
    public class Prestamo
    {
        public int id { get; set; }
        public int CantidadMeses { get; set;}
        public decimal Monto { get; set; }
        public string Plazo { get; set; }
        public string FechaPrestamo { get; set; }
        public string FechaPago { get; set; }


        public int Cuentaid { get; set; }
        public Cuenta Cuenta { get; set; }

    }
}