﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kinal_Banco.Models;

namespace Kinal_Banco.Controllers
{
    public class TipoTarjetasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /TipoTarjetas/
        public ActionResult Index()
        {
            var tipotarjetas = db.TipoTarjetas.Include(t => t.Tarjeta);
            return View(tipotarjetas.ToList());
        }

        // GET: /TipoTarjetas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipotarjeta = db.TipoTarjetas.Find(id);
            if (tipotarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipotarjeta);
        }

        // GET: /TipoTarjetas/Create
        public ActionResult Create()
        {
            ViewBag.Tarjetaid = new SelectList(db.Tarjetas, "id", "FechaCreacion");
            return View();
        }

        // POST: /TipoTarjetas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,tarjetaTipo,Tarjetaid")] TipoTarjeta tipotarjeta)
        {
            if (ModelState.IsValid)
            {
                db.TipoTarjetas.Add(tipotarjeta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Tarjetaid = new SelectList(db.Tarjetas, "id", "FechaCreacion", tipotarjeta.Tarjetaid);
            return View(tipotarjeta);
        }

        // GET: /TipoTarjetas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipotarjeta = db.TipoTarjetas.Find(id);
            if (tipotarjeta == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tarjetaid = new SelectList(db.Tarjetas, "id", "FechaCreacion", tipotarjeta.Tarjetaid);
            return View(tipotarjeta);
        }

        // POST: /TipoTarjetas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,tarjetaTipo,Tarjetaid")] TipoTarjeta tipotarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipotarjeta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Tarjetaid = new SelectList(db.Tarjetas, "id", "FechaCreacion", tipotarjeta.Tarjetaid);
            return View(tipotarjeta);
        }

        // GET: /TipoTarjetas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipotarjeta = db.TipoTarjetas.Find(id);
            if (tipotarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipotarjeta);
        }

        // POST: /TipoTarjetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoTarjeta tipotarjeta = db.TipoTarjetas.Find(id);
            db.TipoTarjetas.Remove(tipotarjeta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
