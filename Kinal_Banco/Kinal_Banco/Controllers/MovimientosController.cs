﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kinal_Banco.Models;

namespace Kinal_Banco.Controllers
{
    public class MovimientosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Movimientos/
        public ActionResult Index()
        {
            var movimientoes = db.Movimientoes.Include(m => m.Transaccion);
            return View(movimientoes.ToList());
        }

        // GET: /Movimientos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimiento movimiento = db.Movimientoes.Find(id);
            if (movimiento == null)
            {
                return HttpNotFound();
            }
            return View(movimiento);
        }

        // GET: /Movimientos/Create
        public ActionResult Create()
        {
            ViewBag.Transaccionid = new SelectList(db.Transaccions, "id", "Lugar");
            return View();
        }

        // POST: /Movimientos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,idCuenta,Lugar,cantidadConsumida,cantidadRetirada,fechaReterido,fechaRealizado,hora,Transaccionid")] Movimiento movimiento)
        {
            if (ModelState.IsValid)
            {
                db.Movimientoes.Add(movimiento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Transaccionid = new SelectList(db.Transaccions, "id", "Lugar", movimiento.Transaccionid);
            return View(movimiento);
        }

        // GET: /Movimientos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimiento movimiento = db.Movimientoes.Find(id);
            if (movimiento == null)
            {
                return HttpNotFound();
            }
            ViewBag.Transaccionid = new SelectList(db.Transaccions, "id", "Lugar", movimiento.Transaccionid);
            return View(movimiento);
        }

        // POST: /Movimientos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,idCuenta,Lugar,cantidadConsumida,cantidadRetirada,fechaReterido,fechaRealizado,hora,Transaccionid")] Movimiento movimiento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(movimiento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Transaccionid = new SelectList(db.Transaccions, "id", "Lugar", movimiento.Transaccionid);
            return View(movimiento);
        }

        // GET: /Movimientos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimiento movimiento = db.Movimientoes.Find(id);
            if (movimiento == null)
            {
                return HttpNotFound();
            }
            return View(movimiento);
        }

        // POST: /Movimientos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Movimiento movimiento = db.Movimientoes.Find(id);
            db.Movimientoes.Remove(movimiento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
