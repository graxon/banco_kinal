﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kinal_Banco.Models;

namespace Kinal_Banco.Controllers
{
    public class TipoMovimientosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /TipoMovimientos/
        public ActionResult Index()
        {
            var tipomovimientoes = db.TipoMovimientoes.Include(t => t.Movimiento);
            return View(tipomovimientoes.ToList());
        }

        // GET: /TipoMovimientos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMovimiento tipomovimiento = db.TipoMovimientoes.Find(id);
            if (tipomovimiento == null)
            {
                return HttpNotFound();
            }
            return View(tipomovimiento);
        }

        // GET: /TipoMovimientos/Create
        public ActionResult Create()
        {
            ViewBag.Movimientoid = new SelectList(db.Movimientoes, "id", "Lugar");
            return View();
        }

        // POST: /TipoMovimientos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,cheque,gastos,transferencia,Movimientoid")] TipoMovimiento tipomovimiento)
        {
            if (ModelState.IsValid)
            {
                db.TipoMovimientoes.Add(tipomovimiento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Movimientoid = new SelectList(db.Movimientoes, "id", "Lugar", tipomovimiento.Movimientoid);
            return View(tipomovimiento);
        }

        // GET: /TipoMovimientos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMovimiento tipomovimiento = db.TipoMovimientoes.Find(id);
            if (tipomovimiento == null)
            {
                return HttpNotFound();
            }
            ViewBag.Movimientoid = new SelectList(db.Movimientoes, "id", "Lugar", tipomovimiento.Movimientoid);
            return View(tipomovimiento);
        }

        // POST: /TipoMovimientos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,cheque,gastos,transferencia,Movimientoid")] TipoMovimiento tipomovimiento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipomovimiento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Movimientoid = new SelectList(db.Movimientoes, "id", "Lugar", tipomovimiento.Movimientoid);
            return View(tipomovimiento);
        }

        // GET: /TipoMovimientos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMovimiento tipomovimiento = db.TipoMovimientoes.Find(id);
            if (tipomovimiento == null)
            {
                return HttpNotFound();
            }
            return View(tipomovimiento);
        }

        // POST: /TipoMovimientos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoMovimiento tipomovimiento = db.TipoMovimientoes.Find(id);
            db.TipoMovimientoes.Remove(tipomovimiento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
